using Microsoft.AspNetCore.Mvc;
using Moq;
using Rest.Controllers;
using Rest.Models;
using System;
using System.Collections.Generic;
using Xunit;
using System.Linq;
using System.Collections.Concurrent;

namespace RestTest
{
    public class StorageControllerTest
    {
        [Fact]
        public void CanAddRecord()
        {
            //����������� �������� ������� RecordModel

            var rec = new AddOrEditRecordModel
            {
                Key = "22",
                Value = "2333"
            };

            //����������� �������� �������������� ���������
            Mock<IRepositoryModel> mock = new Mock<IRepositoryModel>();

            mock.Setup(m => m.AddRecord(It.IsAny<AddOrEditRecordModel>())).Returns(new RecordModel());

            //�����������  �������� ����������� 
            StorageController target = new StorageController(mock.Object);

            //�������� ����� ������ �����������
            target.AddRecord(rec);

            //����������� �������� ���� ��� ��� ������ ����� ���������� 
            //� ��������� � �������� AddOrEditRecordModel
            mock.Verify( m => m.AddRecord(It.IsAny<AddOrEditRecordModel>()));
        }

        [Fact]
        public void CanDeleteRecord()
        {
            //����������� �������� ������� RecordModel

            RecordModel rec = new RecordModel
            {
                Key = "1",
                Value = "2333",
                ChangeDate = DateTime.Now,
                CreateDate = DateTime.Now,
                ViewDate = new List<DateTime> { DateTime.Parse("01.01.2019"), DateTime.Parse("01.02.2019") }
            };

            //����������� �������� �������������� ���������
            Mock<IRepositoryModel> mock = new Mock<IRepositoryModel>();

            mock.Setup(m => m.DeleteRecord(It.IsAny<string>())).Returns(rec.Key);

            //�����������  �������� ����������� 
            StorageController target = new StorageController(mock.Object);

            //�������� - �������� ������
            target.DeleteRecord(rec.Key);

            //����������� �������� ���� ��� ��� ������ ����� �������� ���� ��� � ������ ���� string
            mock.Verify(m => m.DeleteRecord(rec.Key), Times.Exactly(1));
        }

        [Fact]

        public void CanShowNotEmptyRecords()
        {
            //����������� �������� �������������� ���������
            Mock<IRepositoryModel> mock = new Mock<IRepositoryModel>();
            mock.Setup(m => m.ShowNotEmptyValue) ;

            //�����������  �������� ����������� 
            StorageController target = new StorageController(mock.Object);

            //�������� - ��������� ������� � ������� ���� ��������.
            target.ShowNotEmptyRecords();

            //����������� �������� ���� ��� ��� ������ ����� ��������� ���� ������� � ������� ���� ��������
            //� ��������� � ���������� �������� RecordModel
            mock.Verify(m => m.ShowNotEmptyValue);
        }


        [Fact]
        public void CanEditRecord()
        {
            //����������� �������� �������������� ���������
            Mock<IRepositoryModel> mock = new Mock<IRepositoryModel>();

            var editRecord = new AddOrEditRecordModel
            {
                Key = "1",
                Value = "editValue"
            };

            mock.Setup(m => m.EditRecord(It.IsAny<AddOrEditRecordModel>())).Returns(new RecordModel());

            //�����������  �������� ����������� 
            StorageController target = new StorageController(mock.Object);

            //�������� - ���������� ������
            target.EditRecord(editRecord);

            //�����������            
            mock.Verify(m => m.EditRecord(editRecord), Times.Exactly(1));
        }


        [Fact]
        public void ErrorAddRecord()
        {
            //����������� �������� ������� RecordModel

            var rec = new AddOrEditRecordModel
            {
                Key = null,
                Value = "2333"
            };

            //����������� �������� �������������� ���������
            var  mock = new Mock<IRepositoryModel>();


            mock.Setup(m => m.AddRecord(It.IsAny<AddOrEditRecordModel>())).Throws<NullReferenceException>();

            //�����������            
            var repository = new StorageController(mock.Object);

            //�������� - ���������� ������
            repository.AddRecord(rec);
        }

        [Fact]
        public void GetCountRequestByDate()
        {
            //����������� �������� �������������� ���������
            var newRecords = new List<RecordModel>()
            {
                new RecordModel
                {
                    Key = "1",
                    Value = "2333",
                    ViewDate = new List<DateTime>()
                    {
                        DateTime.Parse( "2019-01-17 00:22:05.967"),
                        DateTime.Parse( "2019-01-17 17:00:05.967"),
                        DateTime.Parse( "2019-01-17 17:22:05.967"),
                        DateTime.Parse( "2019-01-17 17:30:05.967"),
                        DateTime.Parse( "2019-01-17 17:30:07.967"),
                        DateTime.Parse( "2019-01-18 17:22:05.967"),
                        DateTime.Parse( "2019-01-20 17:22:05.967"),
                    },
                }
            };

            var filterReturnTwo = new CountRequestModel()
            { Key = "1", StartDate = DateTime.Parse("2019-01-17 17:21:05"), EndDate = DateTime.Parse("2019-01-17 17:30:06") };

            var filterReturnEmpty = new CountRequestModel()
            { Key = "1", StartDate = DateTime.Parse("2018-01-17 17:25:05"), EndDate = DateTime.Parse("2018-01-17 17:22:05") };

            var filterReturnThree = new CountRequestModel()
            { Key = "1", StartDate = DateTime.Parse("2019-01-17 17:00:00"), EndDate = DateTime.Parse("2019-01-17 17:30:06") };

            var mock = new Mock<IConcurrentDictionaryAdapter>();

            mock.Setup(m => m[It.IsAny<string>()]).Returns(newRecords[0]);

            var repository = new RepositoryModel(mock.Object);
            Assert.Equal(2, repository.GetCountRequestByDate(filterReturnTwo));
            Assert.Equal(3, repository.GetCountRequestByDate(filterReturnThree));
            Assert.Equal(0, repository.GetCountRequestByDate(filterReturnEmpty));
        }
    }
}
