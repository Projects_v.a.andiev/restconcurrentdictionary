﻿using Rest.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace RestTest
{
    public class RepositoryMethodTest
    {
        [Fact]
        public void CanAddNewRecord()
        {
            //Организация получение всех записей в dictionary
            var repository = new RepositoryModel(new ConcurrentDictionaryAdapter());

            //Действие - добавление новой записи.            
            var newRecord = repository.AddRecord(
                 new AddOrEditRecordModel { Key = "TestKey", Value = "TestValue" });
            //Утверждение 
            Assert.Equal("TestKey", newRecord.Key);
            Assert.Equal("TestValue", newRecord.Value);
        }

        [Fact]
        public void CanClearRecords()
        {
            //Организация получение всех записей в dictionary
            var repository = new RepositoryModel(new ConcurrentDictionaryAdapter());

            //Действие - добавление новой записи.            
            var newRecord = repository.AddRecord(
                 new AddOrEditRecordModel { Key = "TestKey", Value = "TestValue" });

            //Утверждение
            Assert.True(repository.Records.Count() == 1);
            repository.ClearRecords();
            Assert.Empty(repository.Records);
        }

        [Fact]
        public void CanDeleteRecord()
        {
            //Организация - получение всех записей из dictionary
            var repository = new RepositoryModel(new ConcurrentDictionaryAdapter());            

            //Действие - добавление новой записи.
            var newRecord = new AddOrEditRecordModel()
            {
                Key = "RemoveKey",
                Value = "2333"
            };

            repository.AddRecord(newRecord);

            //Удаление добавленой записи
            repository.DeleteRecord("RemoveKey");

            //Удаление записи
            Assert.Empty(repository.Records.Where(v => v.Key == "RemoveKey"));

        }


        [Fact]
        public void ShowEntriesThatMatter()
        {
            //Организация - получение всех записей из dictionary
            var repository = new RepositoryModel(new ConcurrentDictionaryAdapter());
            repository.ClearRecords();

            //Действие - добавление записей имеющих значение и не имеющих значение

            for (int i = 0; i < 5; i++)
            {
                repository.AddRecord(
                     new AddOrEditRecordModel
                     {
                         Key = i.ToString(),
                         Value = (i >= 3) ? i + "Value" : ""
                     });
            }

            //Две записи с пустыми значениями
            Assert.True(repository.Records.Where(p => !string.IsNullOrWhiteSpace(p.Value)).Count() == 2);

            //Три с заполненными значениями
            Assert.True(repository.Records.Where(p => string.IsNullOrWhiteSpace(p.Value)).Count() == 3);

            //Проверяем как работает наше свойство, действительно ли оно возвращает только записи в которых есть значения.
            Assert.True(repository.ShowNotEmptyValue.Count() == 2);
        }

       

    }
}
