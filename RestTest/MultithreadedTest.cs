﻿using Rest.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using Rest.Controllers;
using Rest.Models.RedisStorage;
using ServiceStack.Redis;

namespace RestTest
{
    public class MultithreadedTest
    {
        [Fact]
        public void CanAddNewRecords()
        {
            //Организация получение всех записей в dictionary
            var repository = new ConcurrentDictionaryAdapter();


            //Действие - добавление новых записей.  
            Task[] tasks = new Task[2];

            tasks[0] = Task.Run(() =>
            {

                for (int i = 1000001; i <= 2000000; i++)
                {
                    repository.TryAdd(
                        i.ToString(),
                         new RecordModel
                         {
                             Key = i.ToString(),
                             Value = "Task1",
                             CreateDate = DateTime.Now
                         });
                };
            });

            tasks[1] = Task.Run(() =>
            {
                for (int i = 0; i <= 1000000; i++)
                {
                    repository.TryAdd(
                         i.ToString(),
                         new RecordModel
                         {
                             Key = i.ToString(),
                             Value = "Task2",
                             CreateDate = DateTime.Now
                         });
                };
            });

            //Ждем результаты задач.
            Task.WaitAll(tasks);

            //Проверка того что есть две записи с лдной датой и разными значениями Task1 и Task2
            //var resultTasks = repository
            // .Values             
            // .GroupBy(c => c.CreateDate)
            // .Where(w => w.Count() > 3 )
            // .Take(1)
            // .SelectMany(c => c.Select(s => ValueTuple.Create(s.Value, s.CreateDate )))             
            // .Distinct();

            var resultTasks = repository
             .Values
             .GroupBy(c => c.CreateDate)
             .Select(s => new { createDate = s.Key, taskCount = s.Count(), recordModel = s.Select(e => e) })
             .Where(w => w.taskCount > 1);

             ;

            //Утверждение 
            Assert.True(resultTasks.Count()>10);

        }

        [Fact]
        public void CanIncrementCountRequestInConccurent()
        {
            //Организация получение всех записей в dictionary          

            var repository = new RepositoryModel(new ConcurrentDictionaryAdapter());

            var testRecord = new AddOrEditRecordModel
                         {
                             Key = "1",
                             Value = "Task1"                             
                         };

            repository.AddRecord(testRecord);


            //Действие - добавление новых записей.  
            Task[] tasks = new Task[5];
            int countIterations = 1000000;

            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = Task.Run(() =>
                {
                    LoopMethodGetRecord(repository.GetRecord, countIterations);
                });
            }

            //Ждем результаты задач.long          
            Task.WaitAll(tasks);

            var countRequest = repository.Records.Where(r => r.Key == "1").Select(s => s.CountRequest).FirstOrDefault();
            Assert.Equal(5000000,countRequest);

        }

      
        public void LoopMethodGetRecord(Func<string, RecordModel> method, int countIterations)
        {
            for (int i = 0; i < countIterations; i++)
            {
                method("1");
            }
        }

        /// <summary>
        /// The free-quota limit on '6000 Redis requests per hour' 
        /// </summary>
        [Fact]
        public void CanIncrementCountrequestInRedis()
        {
            //Организация получение всех записей в dictionary          

            var repository = new RepositoryRedisModel(new RedisIDatabase (new RedisManagerPool("localhost"), new RedisNativeClient()));

            repository.DeleteRecord("1");

            var testRecord = new AddOrEditRecordModel
            {
                Key = "1",
                Value = "Task1"
            };

            repository.AddRecord(testRecord);


            //Действие - добавление новых записей.  
            Task[] tasks = new Task[5];
            int countIterations = 10;

            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = Task.Run(() =>
                {
                    LoopMethodGetRecord(repository.GetRecord, countIterations);
                });
            }
            //Ждем результаты задач.long          
            Task.WaitAll(tasks);

            var countRequest = repository.Records.Where(r => r.Key == "1").Select(s => s.CountRequest).FirstOrDefault();
            Assert.Equal(50, countRequest);

        }

    }

}
