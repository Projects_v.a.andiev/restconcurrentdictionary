﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rest.Models
{
    public class RepositoryModel : IRepositoryModel
    {
        private IConcurrentDictionaryAdapter Storage { get; }
        public RepositoryModel(IConcurrentDictionaryAdapter storage)
        {
            Storage = storage;
        }

        public void SetupSampleData ()
        {
            Storage["1"] = new RecordModel
            {
                Key = "1",
                Value = "2",
                ChangeDate = DateTime.Now,
                CreateDate = DateTime.Now,
                ViewDate = ViewDateList(),
                CountRequest = 6
            };

            Storage["sadsa"] = new RecordModel
            {
                Key = "sadsa",
                Value = "2",
                ChangeDate = DateTime.Now,
                CreateDate = DateTime.Now,
                ViewDate = ViewDateList(),
                CountRequest = 6
            };
        }

        public static List<DateTime> ViewDateList()
        {
            var randomDay = new Random();
            List<DateTime> dateList = new List<DateTime> { };
            for (int i = 0; i < 5; i++)
            {
                dateList.Add(DateTime.Now.AddDays(-randomDay.Next(10)));
            }
            dateList.Add(DateTime.Now);

            return dateList;
        }

        public IEnumerable<RecordModel> Records => Storage.Values;

        public IEnumerable<RecordModel> ShowNotEmptyValue
        {
            get => Storage.Values
                .Where(p => !string.IsNullOrWhiteSpace(p.Value))
                .ToList();
        }

        object lockerGetRecord = new object();
        public RecordModel GetRecord(string key)
        {
            try
            {
                var record = Storage[key];
                
                lock (lockerGetRecord)
                {
                    if (Storage.TryGetValue(record.Key, out record))
                    {
                        {
                            return
                                Storage.AddOrUpdate(
                                    record.Key,
                                    record,
                                    (k, v) =>
                                    {
                                        record.CountRequest = ++v.CountRequest;
                                        record.ViewDate = v.ViewDate;
                                        record.ViewDate.Add(DateTime.Now);
                                        return record;
                                    });
                        }

                    }
                }
                throw new ArgumentException($"Запись с ключом {key} не найдена.");
            }
            catch (KeyNotFoundException)
            {
                throw new ArgumentException($"Запись с ключом {key} не найдена.");
            }
        }



        public RecordModel AddRecord(AddOrEditRecordModel record)
        {
            var newRecord = new RecordModel
            {
                Key = record.Key,
                Value = record.Value,
                CreateDate = DateTime.Now,
                ViewDate = new List<DateTime>()
            };



            bool result = Storage.TryAdd(
                record.Key,
                 newRecord);

            if (result)
            {
                return newRecord;
            }

            throw new ArgumentException($"Запись с ключом уже существуюет - {record.Key}");
        }

        public string DeleteRecord(string key)
        {
            try
            {
                var value = Storage[key];
                var isDelete = Storage.TryRemove(key, out value);

                if (isDelete)
                {
                    return $"Запись с ключом {key} удалена.";
                }
                throw new ArgumentException($"Запись с ключом {key} не найдена.");
            }
            catch (KeyNotFoundException)
            {
                throw new ArgumentException($"Запись с ключом {key} не найдена.");
            }
        }

        public RecordModel EditRecord(AddOrEditRecordModel record)
        {
            try
            {
                var outRecord = Storage[record.Key];
                var locker = new Object();
                lock (locker)
                {
                    if (Storage.TryGetValue(record.Key, out outRecord))
                    {
                        return
                            Storage.AddOrUpdate(
                                record.Key,
                                outRecord,
                                (k, v) =>
                                {
                                    outRecord.Value = record.Value;
                                    outRecord.ChangeDate = DateTime.Now;
                                    outRecord.CountRequest = ++v.CountRequest;
                                    outRecord.ViewDate = v.ViewDate;
                                    outRecord.ViewDate.Add(DateTime.Now);
                                    outRecord.CreateDate = v.CreateDate;
                                    return outRecord;
                                });
                    }
                }
                throw new ArgumentNullException($"Не найдена запись для редактирования с ключом - {record.Key}");
            }
            catch (Exception)
            {
                throw new ArgumentNullException($"Не найдена запись для редактирования с ключом - {record.Key}");
            }
           
        }

        public int GetCountRequestByDate(CountRequestModel filter)
        {
            return Storage[filter.Key].ViewDate.Where(c => c >= filter.StartDate && c <= filter.EndDate).Count();
        }

        public void ClearRecords()
        {
            Storage.Clear();
        }
    }
}
