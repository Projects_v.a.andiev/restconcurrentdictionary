﻿using System;
using System.Collections.Generic;

namespace Rest.Models
{
    /// <summary>
    /// Синтетическое усложнение кода адаптером, чтобы потом мокнуть зависимость коллекцию записей. 
    /// И проверить методы другой зависимости.
    /// </summary>
    public interface IConcurrentDictionaryAdapter
    {
        RecordModel this[string key] { get; set; }
        IEnumerable<RecordModel> Values { get; }
        bool TryGetValue(string key, out RecordModel record);
        RecordModel AddOrUpdate(string key, RecordModel record, Func<string, RecordModel, RecordModel> p);
        bool TryRemove(string key, out RecordModel value);
        bool TryAdd(string key, RecordModel value);
        void Clear();

        bool TryUpdate(string key, RecordModel newValue, RecordModel comparsionValue);
    }
}
