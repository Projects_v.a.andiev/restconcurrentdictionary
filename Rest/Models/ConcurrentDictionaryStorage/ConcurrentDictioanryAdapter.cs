﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;
namespace Rest.Models
{
    /// <summary>
    /// Синтетическое усложнение кода адаптером, чтобы потом мокнуть зависимость коллекцию записей. 
    /// И проверить методы другой зависимости.
    /// </summary>
    public class ConcurrentDictionaryAdapter : IConcurrentDictionaryAdapter
    {
        private readonly ConcurrentDictionary<string, RecordModel> _concurrentDictionary = new ConcurrentDictionary<string, RecordModel>(); 
        public RecordModel this[string key] { get => _concurrentDictionary[key]; set => _concurrentDictionary[key] = value; }

        public IEnumerable<RecordModel> Values { get => _concurrentDictionary.Values; }

        public RecordModel AddOrUpdate(string key, RecordModel record, Func<string, RecordModel, RecordModel> p)
        {
            return _concurrentDictionary.AddOrUpdate(key, record, p);
        }

        public void Clear()
        {
            _concurrentDictionary.Clear();
        }

        public bool TryAdd(string key, RecordModel value)
        {
           return _concurrentDictionary.TryAdd(key, value);
        }

        public bool TryGetValue(string key, out RecordModel record)
        {
            return _concurrentDictionary.TryGetValue(key, out record);
        }

        public bool TryRemove(string key, out RecordModel value)
        {
            return _concurrentDictionary.TryRemove(key, out value); 
        }

        public bool TryUpdate(string key, RecordModel newValue, RecordModel comparsionValue)
        {
            return _concurrentDictionary.TryUpdate(key, newValue, comparsionValue);
        }
    }
}
