﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rest.Models
{
    public interface IRepositoryModel
    {
        /// <summary>
        /// Удаление записи и возвращение ключа записи которую удалили
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string DeleteRecord(string key);

        /// <summary>
        /// Добавление новой записи и возврат добавленой записи
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        RecordModel AddRecord(AddOrEditRecordModel record);

        /// <summary>
        /// Редактирование записи и возврат записи которую отредактировали
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        RecordModel EditRecord(AddOrEditRecordModel record);

        /// <summary>
        /// Выбор записей у которых не пустое значение
        /// </summary>
        IEnumerable<RecordModel> ShowNotEmptyValue { get; }

        /// <summary>
        /// Все записи которые есть в хранилище
        /// </summary>
        IEnumerable<RecordModel> Records { get; }

        /// <summary>
        /// Получение записи по ключу и возврат полученной записи
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        RecordModel GetRecord(string key);

        /// <summary>
        /// Получение количества обращений к записи
        /// </summary>
        /// <param name="countReq"></param>
        /// <returns></returns>
        int GetCountRequestByDate(CountRequestModel countReq);

        void SetupSampleData();

    }
}
