﻿using System.Net;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Redis;

namespace Rest.Models.RedisStorage
{
    public class RedisIDatabase
    {
        private readonly IRedisClient _redisDatabase;
        private readonly IRedisNativeClient _redisNativeClient;
        public RedisIDatabase(RedisManagerPool multiplexer, RedisNativeClient redisNativeClient)
        {
            _redisDatabase = multiplexer.GetClient();
            _redisNativeClient = redisNativeClient;
           
        }

        public IRedisClient DataBase { get => _redisDatabase; }

        public IRedisNativeClient NativeClient { get => _redisNativeClient; }

        
    }
}