﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Rest.Models.RedisStorage;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

namespace Rest.Models
{
    public class RepositoryRedisModel : IRepositoryModel
    {
        private RedisIDatabase Storage { get; }

        public RepositoryRedisModel(RedisIDatabase storage)
        {
            Storage = storage;
        }

        public IEnumerable<RecordModel> Records
        {
            get
            {
                var allRecords = Storage.DataBase.ScanAllHashEntries("records:value");
               
                var listRecord = new List<RecordModel>();
                foreach (var keyValue in allRecords)
                {
                    listRecord.Add(GetDeserializeRecordModel(Storage.DataBase.GetValuesFromHash($"records:record:{keyValue.Key}", "recordmodel")[0]));
                }
                return listRecord;
            }
        }

        public IEnumerable<RecordModel> ShowNotEmptyValue
        {
            get
            {
                var allRecords = Storage.DataBase.ScanAllHashEntries("records:value");

                var notEmptyRecords = allRecords.Where(w => !string.IsNullOrWhiteSpace(w.Value));
                var listRecord = new List<RecordModel>();
                foreach (var keyValue in notEmptyRecords)
                {
                    listRecord.Add(GetDeserializeRecordModel(Storage.DataBase.GetValuesFromHash($"records:record:{keyValue.Key}", "recordmodel")[0]));
                }
                return listRecord;
            }
        }


        private RecordModel GetDeserializeRecordModel(string jsonRecordModel)
        {
            return JsonConvert.DeserializeObject<RecordModel>(jsonRecordModel);
        }

        private RecordModel GetRecordModelObj(string recordKey)
        {
            var newRecord = new RecordModel
            {
                Key = Storage.DataBase.GetValueFromHash($"records:record:{recordKey}", "key"),
                Value = Storage.DataBase.GetValueFromHash($"records:record:{recordKey}", "value"),
                ChangeDate = DateTime.Parse(Storage.DataBase.GetValueFromHash($"records:record:{recordKey}", "changedate")),
                CreateDate = DateTime.Parse(Storage.DataBase.GetValueFromHash($"records:record:{recordKey}", "createdate")),
                CountRequest = Convert.ToInt64(Storage.DataBase.GetValueFromHash($"records:record:{recordKey}", "countrequest")),
                ViewDate = new List<DateTime>()
            };

            newRecord.ViewDate = Storage.DataBase.GetAllItemsFromList($"records:viewdate:{recordKey}").Select(s => DateTime.Parse(s)).ToList();

            Storage.DataBase.SetEntryInHash($"records:record:{recordKey}", "recordmodel", JsonConvert.SerializeObject(newRecord));

            return newRecord;
        }

        public RecordModel AddRecord(AddOrEditRecordModel record)
        {
            var newRecord = new RecordModel
            {
                Key = record.Key,
                Value = record.Value,
                CreateDate = DateTime.Now,
                ViewDate = new List<DateTime>()
            };



            using (var trans = Storage.DataBase.CreateTransaction())
            {
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "recordmodel", JsonConvert.SerializeObject(newRecord)));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "value", newRecord.Value));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "countrequest", newRecord.CountRequest.ToString()));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "createdate", newRecord.CreateDate.ToString()));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "changedate", newRecord.ChangeDate.ToString()));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "key", newRecord.Key.ToString()));

                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:value", newRecord.Key, newRecord.Value));

                trans.Commit();
            }
            return newRecord;
        }


        object lockerGetRecord = new object();
        public RecordModel GetRecord(string key)
        {

            long callbackResult;

            lock (lockerGetRecord)
            {
                using (var trans = Storage.DataBase.CreateTransaction())
                {
                    trans.QueueCommand(r => r.IncrementValueInHash($"records:record:{key}", "countrequest", 1), i => callbackResult = i);
                    trans.QueueCommand(r => r.AddItemToList($"records:viewdate:{key}", DateTime.Now.ToString()));
                    //trans.QueueCommand(r => r.IncrementItemInSortedSet($"records:record:viewdate:{key}", DateTime.Now.ToString(), 1));
                    trans.Commit();
                }


                Storage.DataBase.RemoveEntryFromHash($"records:record:{key}", "recordmodel");

                return GetRecordModelObj(key);
            }
        }

        public string DeleteRecord(string key)
        {
            Storage.DataBase.Remove($"records:record:{key}");
            Storage.DataBase.RemoveEntryFromHash($"records:value", key);
            return $"Запись с ключом {key} удалена";
        }

        public RecordModel EditRecord(AddOrEditRecordModel record)
        {
            using (var trans = Storage.DataBase.CreateTransaction())
            {

                trans.QueueCommand(r => r.RemoveEntryFromHash($"records:value", record.Key));

                trans.QueueCommand(r => r.RemoveEntryFromHash($"records:record:{record.Key}", "changedate"));
                trans.QueueCommand(r => r.RemoveEntryFromHash($"records:record:{record.Key}", "value"));

                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:value", record.Key, record.Value));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{record.Key}", "value", record.Value));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{record.Key}", "changedate", DateTime.Now.ToString()));
                trans.Commit();
            }

            return GetRecordModelObj(record.Key);
        }

        public int GetCountRequestByDate(CountRequestModel countReq)
        {
            var listDate = Storage.DataBase.GetAllItemsFromList($"records:viewdate:{countReq.Key}");
            var resultCountRequestByDate = listDate.Where(w => DateTime.Parse(w) >= countReq.StartDate && DateTime.Parse(w) <= countReq.EndDate).Count();
            return resultCountRequestByDate;
        }

        void IRepositoryModel.SetupSampleData()
        {
            SetupSampleData();
        }


        public void SetupSampleData()
        {
            var firstRecord = new RecordModel
            {
                Key = "1",
                Value = "2",
                ChangeDate = DateTime.Now,
                CreateDate = DateTime.Now,
                ViewDate = ViewDateList(),
                CountRequest = 6
            };

            var secondRecord = new RecordModel
            {
                Key = "sadsa",
                Value = "2",
                ChangeDate = DateTime.Now,
                CreateDate = DateTime.Now,
                ViewDate = ViewDateList(),
                CountRequest = 6
            };

            InitializeAddSimpleData(firstRecord);
            InitializeAddSimpleData(secondRecord);
        }

        private void InitializeAddSimpleData(RecordModel newRecord)
        {
            using (var trans = Storage.DataBase.CreateTransaction())
            {
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "recordmodel", JsonConvert.SerializeObject(newRecord)));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "value", newRecord.Value));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "countrequest", newRecord.CountRequest.ToString()));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "createdate", newRecord.CreateDate.ToString()));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "changedate", newRecord.ChangeDate.ToString()));
                trans.QueueCommand(r => r.SetEntryInHashIfNotExists($"records:record:{newRecord.Key}", "key", newRecord.Key.ToString()));

                foreach (var viewDate in newRecord.ViewDate)
                {
                    trans.QueueCommand(r => r.AddItemToList($"record:viewdate:{newRecord.Key}", viewDate.ToString()));
                }

            }

        }



        public static List<DateTime> ViewDateList()
        {
            var randomDay = new Random();
            List<DateTime> dateList = new List<DateTime> { };
            for (int i = 0; i < 5; i++)
            {
                dateList.Add(DateTime.Now.AddDays(-randomDay.Next(10)));
            }
            dateList.Add(DateTime.Now);

            return dateList;
        }


        private static byte[] ToByteArray<T>(T obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        private static T FromByteArray<T>(byte[] data)
        {
            if (data == null)
                return default(T);
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream(data))
            {
                object obj = bf.Deserialize(ms);
                return (T)obj;
            }
        }
    }
}
