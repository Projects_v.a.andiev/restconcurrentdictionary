﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Rest.Models
{
    [Serializable]
    /// <summary>
    /// Модель для передачи данных при обращении к методам контроллера, так как редактировать пока можно два поля, а не все что находятся в полной модели 
    /// вынес два редактируемых поля в отдельную модель
    /// </summary>
    public class AddOrEditRecordModel
    {
        
        [Required]
        public string Key { get; set; }
       
        public string Value { get; set; }
    }
}
