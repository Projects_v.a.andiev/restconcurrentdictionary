﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Rest.Models;

namespace Rest.Controllers
{
    [Route("api")]
    [ApiController]
    public class StorageController : ControllerBase
    {
        private readonly IRepositoryModel _repModel;

        public StorageController(IRepositoryModel repModel)
        {
            _repModel = repModel;
            //_repModel.SetupSampleData();
        }

        /// <summary>
        /// Получить записи у которых не пустые значения
        /// </summary>
        /// <returns></returns>
        [HttpGet("ShowNotEmptyValue")]
        public ActionResult<IEnumerable<RecordModel>> ShowNotEmptyRecords()
        {
            try
            {
                return Ok(_repModel.ShowNotEmptyValue);
            }
            catch (Exception e)
            {
                return this.NotFound(e.Message);
            }
            
        }

        /// <summary>
        /// Получить значение записи по ключу
        /// </summary>
        /// <remarks>
        /// 1
        /// </remarks>
        /// <param name="key"></param>
        /// <returns></returns>
        // GET api/values/5      
        [HttpGet("GetRecord/{key}")]
        public IActionResult GetRecord(string key)
        {
            try
            {
                return Ok(_repModel.GetRecord(key));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Добавление записи в хранилище
        /// </summary>
        /// <remarks>
        /// Пример
        ///{"key":"1","value":"1"}
        /// </remarks>
        /// <param name="record"></param>
        /// <returns></returns>        
        [HttpPost("AddRecord")]
        public IActionResult AddRecord([FromBody]AddOrEditRecordModel record)
        {
            try
            {
                return Ok(_repModel.AddRecord(record));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Редактирование записи
        /// </summary>
        /// <remarks>
        /// Пример редактирования записи
        /// {
        ///     "key":"1",
        ///     "value": "TestUpdate"
        /// }                
        /// </remarks>
        /// <param name="record"></param>
        /// <returns></returns>
        // PUT api/values/5        
        [HttpPut("EditRecord")]
        public IActionResult EditRecord(AddOrEditRecordModel record)
        {
            try
            {
                return Ok(_repModel.EditRecord(record));
            }
            catch (Exception e)
            {

                return this.NotFound(e.Message);
            }
        }

        /// <summary>
        /// Удаление записи по ключу
        /// </summary>
        /// <remarks>
        /// 1
        /// </remarks>
        /// <param name="key"></param>
        /// <returns></returns>
        // DELETE api/values/5
        [HttpDelete("DeleteRecord")]
        public IActionResult DeleteRecord([FromBody]string key)
        {
            try
            {
                return Ok(_repModel.DeleteRecord(key));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Получить количество обращений к записи по временному интервалу
        /// </summary>
        /// <remarks>
        /// {
        ///     "Key": "1",
        ///     "StartDate": ": "01.20.2018",
        ///     "EndDate": ": "01.20.2019"
        /// }
        /// </remarks>
        /// <param name="filter"></param>
        /// <returns></returns>       
        [HttpGet("GetCountRequestByDate")]
        public IActionResult GetCountRequestByDate([FromQuery]CountRequestModel filter)
        {
            try
            {
               
                return Ok(_repModel.GetCountRequestByDate(filter));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
