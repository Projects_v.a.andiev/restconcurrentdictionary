﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Rest.Models;
using Rest.Models.RedisStorage;
using ServiceStack.Redis;

using Swashbuckle.AspNetCore.Swagger;

namespace Rest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }
       

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var appMode = this.Configuration.GetSection("ApplicationMode").Value;

            if (appMode == "dictionary")
            {
                services.AddSingleton<IRepositoryModel, RepositoryModel>(x => new RepositoryModel(new ConcurrentDictionaryAdapter()));
            }
            else if (appMode == "redis")
            {
                services.AddTransient<IRepositoryModel, RepositoryRedisModel>(x => new RepositoryRedisModel(
                 new RedisIDatabase(new RedisManagerPool("localhost"), new RedisNativeClient())));
            }

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("Storage", new Info
                {
                    Version = "v1",
                    Title = "Storage",
                    Description = "A simple example ASP.NET Core Web API"
                });

                c.MapType<System.DateTime>(() => new Swashbuckle.AspNetCore.Swagger.Schema
                {
                    Type = "string",
                    Format = "MM/dd/yyyy HH:mm:ss"
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            //services.AddDistributedRedisCache(option =>
            //{
            //    option.Configuration = "127.0.0.1";
            //    option.InstanceName = "master";
                
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseStaticFiles();

            
            app.UseSwagger();
           
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/Storage/swagger.json", "Storage");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
